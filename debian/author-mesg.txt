To: Fumitoshi UKAI <ukai@debian.or.jp>
Subject: Re: copyright of a12k12
From: Yutaka Tsuboi <yutaka@mcs.meitetsu.co.jp>
Message-Id: <199908230035.AA00038@hibiscus.mcs.meitetsu.co.jp>
Date: Mon, 23 Aug 1999 09:35:59 +0900
In-Reply-To: <14266.59375.59143.61791G@lavender.ukai.org>
References: <14266.59375.59143.61791G@lavender.ukai.org>
MIME-Version: 1.0
X-Mailer: AL-Mail32 Version 1.10
Content-Type: text/plain; charset=ISO-2022-JP
X-Filter: Fumi::Agent 1.29 for ukai
Content-Transfer-Encoding: 7bit

$B$O$8$a$^$7$F!#DZ0f!wL>E4%3%s%T%e!<%?$G$9!#(B

In message "copyright of a12k12",
Fumitoshi UKAI wrote...

>1) $B$b$H$b$H$N(Boriginal$B$N(Barchive$B$O$I$3$K$"$k$N$G$7$g$&$+(B?

$B$b$H$b$H!"(Ba12k12$B$O!"(BUNIX$B$NJY6/$HIa5Z$rL\;X$7$F$$$?!"<RFb$N(B
$BM-;V$G!"%N!<%H%Q%=%3%s$G$b2wE,$J4D6-$r$H:n@.$5$l$?$b$N$G!"(B
$BEv=i$+$i!"@Q6KE*$K8x3+$9$k$D$b$j$O$J$+$C$?$N$G$9!#(B
$B$=$l$r!";d$,(B Nifty $B$G(B PANIX $B8~$1$N%G!<%?$H$7$F8x3+$7$?$N$,(B
$BH/C<$H$J$C$F$*$j!"$"$k0UL#$G$O(B original $B$O(B nifty $B$H$b8@$((B
$B$^$9$,!"$3$3$O;DG0$J$,$i2q0w@)$G$9!#(B
$B$^$?!"$=$&$$$&@8$$N)$A$G$9$N$G!":#$N$H$3$m$I$3$+$G8x3+$7$F(B
$B$$$k$H8@$&$3$H$O$J$$$N$G$9!#$I$3$+!"C5$=$&$+$H$O9M$($F$O$$(B
$B$k$N$G$9$1$I$M!#(B

>2) JP archives $B$K$"$k$b$N$K$O(B
$B!'CfN,(B
>http://www.jp.debian.org/social_contract#guidelines
>$B$G$$$&$H$3$m$N!V%U%j!<!W$G$7$g$&$+(B?

$BFI$^$7$F$$$?$@$-$^$7$?$,!"$3$N0UL#$G%U%j!<$GNI$$$+$H;W$$$^(B
$B$9!#(B

>$BNc$($P!"(BX11$BImB0$N%U%)%s%H$HF1$8$h$&$J(Bcopyright$B$H9M$($F$*$1$P(B
>$B$h$m$7$$$N$G$7$g$&$+(B?

$B$O$$!"$=$NDL$j$G$9!#(B

p.s.
$B$3$l$@$19b2rA|EY$N%^%7%s$,Ia5Z$7$F$$$k$N$G!";d$I$b$O!"(B12$B%I%C(B
$B%H%U%)%s%H$J$s$F!"2a5n$NJ*$H$H$i$($F$$$?=j$b$"$j!":G6a$NH?6A(B
$B$K$O>/!96C$$$F$$$^$9!#(B

         ---------------------------------------------------
          $BDZ0f(B $BM5(B      $BL>E4%3%s%T%e!<%?%5!<%S%9(B  $BBh#2;v6HIt(B
         ---------------------------------------------------
                | E-mail  yutaka@mcs.meitetsu.co.jp |
                | Nifty   GGA02631                  |
                | TEL     (052)582-3162             |

